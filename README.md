## "I just want to use it"

### Hardware

- Connect the Clock and Data lines to your microcontroller.**The GPIO pins must be 5V compatible**

- Connect the Clock and Data lines to 5V through a 1-10k Ohm resistor

- Configure your GPIO pins to open drain output

- Configure a 1MHz timer interrupt


### Software

- Add the library files to your project

- Declare a keyboard struct

- Implement and map all the function pointers from the keyboard struct, as shown in the example

- Add the timerCallback function to your timer interrupt callback

- Call the keyboardInit function before your while(1) loop

- Call the checkHostRequest function in you while(1) loop

Once you do these, you should be able to send ascii characters with charKeyPress, send PS2 scan codes with sendKey or send bytes with keyboardSendMSG (only ascii characters 32-126 are implemented).

## To be done

###### Replies to Host

At this stage, the device does not check every plausible command from the host. It just replies with an Ack and sends a self test report when it receives an error. It should check for more things such as caps lock status.

###### Multimedia codes

The PS2 protocol supports more than the standard ascii characters. Usefull things such as volume control, track control, browser back-forward could be defined in the header file for ease of use.

## More reading

This project is based on [this](https://oe7twj.at/images/6/6a/PS2_Keyboard.pdf) excellent article from Adam Chapweske. If you need more information on the project, I highly recommend reading it
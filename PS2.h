/*
 * PS2.h
 *
 *  Created on: Feb 21, 2023
 *      Author: iliaz
 */

#ifndef INC_PS2_H_
#define INC_PS2_H_

#include "main.h"

#define TIME_BETWEEN_MESSAGES 1

struct actions{
	void (*setCLKHigh)();
	void (*setCLKLow)();
	void (*toggleCLK)();
	void (*setDataHigh)();
	void (*setDataLow)();
	uint8_t (*readCLK)();
	uint8_t (*readData)();
	void (*startInt)();
	void (*stopInt)();
};
struct keyboard{
	uint8_t data;
	uint8_t counter;
	uint8_t parityCounter;
	uint8_t dataSent;
	uint8_t receivedData;
	uint8_t send1_receive0;
	uint8_t abort;
	uint8_t hostRequestSequenceCounter;
	uint32_t lastMessageTime;
	uint32_t (*time)();
	struct actions actions;
};

extern uint8_t asciiToPS2[128];
extern uint64_t shiftFlags[2];

void keyboardInit(struct keyboard *device);
void timerCallback(struct keyboard *device);
void keyboardSendMSG(uint8_t data,struct keyboard *keyboard, uint32_t timeout,uint8_t numOfTries);
void keyboardReceiveMSG(struct keyboard *device, uint32_t timeout);
void sendKey(struct keyboard *device, uint8_t* makeBytes, uint8_t makeLength, uint8_t* breakBytes, uint8_t brakeLength, uint8_t shiftPressed);
void charKeyPress(struct keyboard *device,uint8_t character);
void checkHostRequest(struct keyboard *device);

#endif /* INC_PS2_H_ */

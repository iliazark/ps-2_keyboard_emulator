/*
 * PS2.c
 *
 *  Created on: Feb 21, 2023
 *      Author: iliaz
 */


#include "PS2.h"

uint8_t asciiToPS2[128]={0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,
						0,0,0,0,0,0,0,0,0,0,
						0,0,0x29,0x16,0x52,0x26,0x25,0x2E,0x3D,0x52,
						0x46,0x45,0x3E,0x55,0x41,0x4E,0x49,0x4A,0x45,0x16,
						0x1E,0x26,0x25,0x2E,0x36,0x3D,0x3E,0x46,0x4C,0x4C,
						0x41,0x55,0x49,0x4A,
						0x1E,0x1C,0x32,0x21,0x23,0x24,0x2B,0x34,0x33,0x43,
						0x3B,0x42,0x4B,0x3A,0x31,0x44,0x4D,0x15,0x2D,0x1B,
						0x2C,0x3C,0x2A,0x1D,0x22,0x35,0x1A,0x54,0x5D,0x5B,
						0x36,0x4E,0x0E,0x1C,0x32,0x21,0x23,0x24,0x2B,0x34,
						0x33,0x43,0x3B,0x42,0x4B,0x3A,0x31,0x44,0x4D,0x15,
						0x2D,0x1B,0x2C,0x3C,0x2A,0x1D,0x22,0x35,0x1A,0x54,
						0x5D,0x5B,0x0E,0};
uint64_t shiftFlags[2]={0B0000000000000000000000000000000001111110111100000000000000101011
						,0B1111111111111111111111111110001100000000000000000000000000011110};

/*
 * This function is called once in the beginning
 * to initialize variables and io
 * */
void keyboardInit(struct keyboard *device){
	device->actions.setCLKHigh();
	device->actions.setDataHigh();
	device->hostRequestSequenceCounter=0;
	device->lastMessageTime=0;
}

/*
 * This function handles the per byte communication.
 * It should run when keyboard.actions.startInt() is called
 * and stopped when the keyboard.actions.start() is called.
 * Tested @1Mhz*/
void timerCallback(struct keyboard *device){
	if (device->send1_receive0 == 1) {
		//sending a message (byte)
		if (device->counter > 35) {
			if (device->counter == 36) {
				if (device->parityCounter % 2) {
					device->actions.setDataLow();
//					HAL_GPIO_WritePin(PS2_DATA_GPIO_Port, PS2_DATA_Pin,GPIO_PIN_RESET);
				} else {
					device->actions.setDataHigh();
				}
			} else if (device->counter == 37) {
				device->actions.setCLKLow();
			} else if (device->counter == 39) {
				device->actions.setCLKHigh();
			} else if (device->counter == 40) {
				device->actions.setDataHigh();
			} else if (device->counter == 41) {
				device->actions.setCLKLow();
			} else if (device->counter == 43) {
				device->actions.setCLKHigh();
			} else if (device->counter > 43) {
				device->actions.stopInt();
				device->dataSent = 1;
			}
		} else if (device->counter < 4) {
			if (device->counter == 0) {
				device->actions.setDataLow();
			} else if (device->counter == 1) {
				device->actions.setCLKLow();
			} else if (device->counter == 3) {
				device->actions.setCLKHigh();
				device->parityCounter = 0;
			}
		} else {
			if (!(device->counter % 2)) {
				if (!((device->counter) % 4)) {
					uint8_t mask = 0B00000001;
					mask = mask << ((device->counter - 4) / 4);
					if (device->data & mask) {
						device->actions.setDataHigh();
					} else {
						device->actions.setDataLow();
						device->parityCounter++;
					}
				}

			} else {
				device->actions.toggleCLK();
			}

		}
		device->counter++;
	} else if (device->send1_receive0 == 0) {
		//receiving a message (byte)
		if (device->counter > 39) {
			if (device->counter == 40) {
				device->actions.setDataLow();
			} else if (device->counter == 41) {
				device->actions.toggleCLK();
			} else if (device->counter == 43) {
				device->actions.setCLKHigh();
			} else if (device->counter == 44) {
				device->actions.setDataHigh();
				device->actions.stopInt();
			}
		} else if((device->counter<33) && !(device->counter%4)){
			if(HAL_GPIO_ReadPin(PS2_DATA_GPIO_Port, PS2_DATA_Pin) == GPIO_PIN_SET){
				uint8_t bit = 0B00000001;
				bit=bit<<((device->counter/4)-1);
				device->receivedData |= bit;
			}else{
				uint8_t bit = 0B00000001;
				bit= bit<<((device->counter/4)-1);
				device->receivedData &= ~bit;
			}
		}else {

			if (device->counter % 2) {
				device->actions.toggleCLK();
			}
		}
		device->counter++;
	}
	device->lastMessageTime=device->time();
}

/*
 * This function prepares all the flags and variables
 * that the timer interrupt needs and starts the timer
 * in send mode
 * */
void keyboardSendMSG(uint8_t data,struct keyboard *device, uint32_t timeout,uint8_t numOfTries){
	uint32_t start = device->time();
	device->counter=0;
	device->data=data;
	device->dataSent=0;
	device->send1_receive0=1;
	device->abort=0;
	while(device->lastMessageTime>=(device->time()-TIME_BETWEEN_MESSAGES)){
		if((device->time()-start)>timeout){
			return;
		}
	}
	device->actions.startInt();
	while((!device->dataSent) && (device->time()-start)<timeout && (numOfTries>0)){
		if((device->abort)==1){
			numOfTries--;
			while(HAL_GPIO_ReadPin(PS2_CLK_GPIO_Port, PS2_CLK_Pin) == GPIO_PIN_RESET){
				if((device->time()-start)>timeout){
					return;
				}
			}
			device->abort=0;
		}
	}
}

/*
 * This function prepares all the flags and variables
 * that the timer interrupt needs and starts the timer
 * in receive mode
 * */
void keyboardReceiveMSG(struct keyboard *device, uint32_t timeout){
	device->send1_receive0=0;
	device->counter=0;
	device->actions.startInt();
}

/*
 * This function gets all the bytes for a single key press
 * and sends them one by one
 * */
void sendKey(struct keyboard *device, uint8_t* makeBytes, uint8_t makeLength, uint8_t* breakBytes, uint8_t breakLength, uint8_t shiftPressed){

	//send shift press code
	if(shiftPressed){
		keyboardSendMSG(0x12, device, 1000, 3);
	}

	//send make code
	for(int i=0; i<makeLength; i++){
		keyboardSendMSG(makeBytes[i], device, 1000, 3);
	}

	//send break code
	for(int i=0; i<breakLength; i++){
		keyboardSendMSG(breakBytes[i], device, 1000, 3);
	}

	//send shift break code
	if(shiftPressed){
		keyboardSendMSG(0xF0, device, 1000, 3);
		keyboardSendMSG(0x12, device, 1000, 3);
	}
}

/*
 * This function gets a character,
 * convert it to a PS2 scan code
 * and sends it to the host
 * */
void charKeyPress(struct keyboard *device,uint8_t character){
	if(character<128){
		uint8_t shiftArray=character/64;
		uint64_t flag = 0x8000000000000000;
		flag = flag >> (character - (64*shiftArray));
		uint8_t shift=0;
		if(shiftFlags[shiftArray] & flag){
			shift=1;
		}

		uint8_t breakBytes[2]={0xF0,asciiToPS2[character]};
		sendKey(device, &asciiToPS2[character], 1, breakBytes, 2, shift);
	}

}


/*
 * This function needs to be run at
 * intervals not to exceed 5ms
 * */
void checkHostRequest(struct keyboard *device){
	if((device->time() - device->lastMessageTime)>3){

		//if host inhibits communication
		if((device->actions.readCLK() == 0) && device->hostRequestSequenceCounter==0){
			if((HAL_GPIO_ReadPin(PS2_DATA_GPIO_Port, PS2_DATA_Pin) == 1)){
				device->hostRequestSequenceCounter=1;
			}else{
				device->hostRequestSequenceCounter=0;
			}
		}

		//if host requests to send data
		if((device->actions.readData() == 0) && device->hostRequestSequenceCounter==1){
			if((device->actions.readCLK() == 0)){
				device->hostRequestSequenceCounter=2;
			}else{
				device->hostRequestSequenceCounter=0;
			}

		}

		if((device->actions.readCLK() == 1) && (device->hostRequestSequenceCounter==2 || device->hostRequestSequenceCounter==1)){
			if((device->actions.readData() == 0)){
				device->hostRequestSequenceCounter=0;
				keyboardReceiveMSG(device, 1000);
				HAL_Delay(1);
				keyboardSendMSG(0xFA, device, 1000, 2);
				if(device->receivedData == 0xFF){
					HAL_Delay(2);
					keyboardSendMSG(0xAA, device, 1000, 2);
				}
			}else{
				device->hostRequestSequenceCounter=0;
			}

		}

	}
}
